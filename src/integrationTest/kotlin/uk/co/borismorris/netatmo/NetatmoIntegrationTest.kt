package uk.co.borismorris.netatmo

import com.github.tomakehurst.wiremock.common.Notifier
import com.github.tomakehurst.wiremock.core.Options.ChunkedEncodingPolicy.BODY_FILE
import com.github.tomakehurst.wiremock.core.WireMockConfiguration
import com.github.tomakehurst.wiremock.junit5.WireMockExtension
import com.influxdb.query.dsl.Flux
import io.github.oshai.kotlinlogging.KotlinLogging
import org.assertj.core.api.Assertions.assertThat
import org.awaitility.Awaitility.await
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.RegisterExtension
import org.junit.jupiter.api.io.TempDir
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.DynamicPropertyRegistry
import org.springframework.test.context.DynamicPropertySource
import org.testcontainers.containers.InfluxDBContainer
import org.testcontainers.junit.jupiter.Container
import org.testcontainers.junit.jupiter.Testcontainers
import org.testcontainers.utility.DockerImageName
import uk.co.borismorris.netatmo.influx.InfluxClient
import uk.co.borismorris.netatmo.worker.GatherNetatmoData
import java.nio.file.Files
import java.nio.file.Path
import java.time.Duration
import java.time.temporal.ChronoUnit
import kotlin.io.path.absolutePathString

private val logger = KotlinLogging.logger {}

@SpringBootTest(
    classes = [NetatmoInfluxApplication::class],
    webEnvironment = SpringBootTest.WebEnvironment.NONE,
    properties = [
        "pyroscope.enabled=false",
        "spring.security.oauth2.client.registration.netatmo.client-id=test-client-id",
        "spring.security.oauth2.client.registration.netatmo.client-secret=test-client-secret",
        "spring.security.oauth2.client.registration.netatmo.scope=test-scope-1,test-scope-2",
        "netatmo.credentials.username=test-user",
        "netatmo.credentials.password=test-user-password-123",
        "netatmo.station-polling-interval=PT1H",
        "netatmo.device-polling-interval=PT30S",
        "influx.org=default",
        "influx.bucket=netatmoweather",
        "influx.token=abc123",
        "logging.level.uk.co.borismorris.netatmo.Slf4jNotifier=WARN",
    ],
)
@Testcontainers
class NetatmoIntegrationTest {

    companion object {
        val influxImage = DockerImageName.parse("influxdb")

        @RegisterExtension
        @JvmStatic
        val wiremock: WireMockExtension = WireMockExtension.newInstance()
            .options(
                WireMockConfiguration.wireMockConfig()
                    .dynamicPort()
                    .usingFilesUnderClasspath("uk/co/borismorris/netatmo")
                    .notifier(Slf4jNotifier())
                    .templatingEnabled(true)
                    .globalTemplating(true)
                    .gzipDisabled(false)
                    .useChunkedTransferEncoding(BODY_FILE),
            )
            .configureStaticDsl(true)
            .failOnUnmatchedRequests(true)
            .build()

        @Container
        @JvmStatic
        val influxdb = InfluxDBContainer(influxImage)
            .withUsername("admin")
            .withPassword("password")
            .withOrganization("default")
            .withBucket("netatmoweather")
            .withAdminToken("abc123")
            .withEnv("INFLUXD_LOG_LEVEL", "debug")

        @JvmStatic
        @TempDir
        lateinit var tempDir: Path

        @JvmStatic
        @DynamicPropertySource
        fun registerDysonProperties(registry: DynamicPropertyRegistry) {
            registry.add("netatmo.cache-path") {
                Files.createTempDirectory(tempDir, "token-refresh").resolve("oauth_client_registry.json").absolutePathString()
            }
            registry.add("netatmo.url") { "${wiremock.baseUrl()}/api" }
            registry.add("spring.security.oauth2.client.provider.netatmo.token-uri") { "${wiremock.baseUrl()}/token" }
            registry.add("influx.url") { influxdb.url }
        }
    }

    @Autowired
    lateinit var dataGather: GatherNetatmoData

    @Autowired
    lateinit var influxClient: InfluxClient

    @Test
    fun `I can start the application`() {
        val flux = Flux.from("netatmoweather").range(-10, ChronoUnit.YEARS).toString()
        await()
            .atMost(Duration.ofMinutes(5))
            .pollInSameThread()
            .pollInterval(Duration.ofSeconds(5))
            .untilAsserted {
                val results = influxClient.query(flux).asSequence().flatMap { it.records }.toList()
                assertThat(results).hasSize(36_731)
            }
    }
}

class Slf4jNotifier : Notifier {
    override fun info(message: String?) {
        logger.info { message }
    }

    override fun error(message: String?) {
        logger.error { message }
    }

    override fun error(message: String?, t: Throwable?) {
        logger.error(t) { message }
    }
}
