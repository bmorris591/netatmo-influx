package uk.co.borismorris.netatmo.config.props

import java.net.URI

class NetatmoProps {
    lateinit var url: URI
    val credentials = NetamoCredentials()

    override fun toString() = "NetamoProps(url=$url,, credentials=$credentials)"
}

class NetamoCredentials {
    lateinit var username: String
    lateinit var password: String

    override fun toString() = "NetamoCredentials(username='$username', password='XXXX')"
}
