package uk.co.borismorris.netatmo.logging

import io.github.oshai.kotlinlogging.KotlinLogging
import org.springframework.boot.context.event.ApplicationStartedEvent
import org.springframework.boot.info.BuildProperties
import org.springframework.boot.info.GitProperties
import org.springframework.context.ConfigurableApplicationContext
import org.springframework.context.event.EventListener
import uk.co.borismorris.netatmo.config.props.NetatmoProps

private val logger = KotlinLogging.logger {}

class LogConfigOnStartup(private val context: ConfigurableApplicationContext) {
    @EventListener(ApplicationStartedEvent::class)
    fun onStartup() {
        context.getBean(NetatmoProps::class.java).also { logger.info { "netatmoProps -> $it" } }
    }
}

class LogVersionOnStartup(val buildProperties: BuildProperties?) {
    @EventListener(ApplicationStartedEvent::class)
    fun onStartup() {
        buildProperties?.apply { logger.info { "$group:$artifact:$version@$time" } }
    }
}

class LogGitInfoOnStartup(val gitProperties: GitProperties?) {
    @EventListener(ApplicationStartedEvent::class)
    fun onStartup() {
        gitProperties?.apply { logger.info { "$branch@$commitId" } }
    }
}
