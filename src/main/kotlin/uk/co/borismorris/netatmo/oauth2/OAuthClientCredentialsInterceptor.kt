package uk.co.borismorris.netatmo.oauth2

import org.springframework.http.HttpHeaders
import org.springframework.http.HttpRequest
import org.springframework.http.client.ClientHttpRequestExecution
import org.springframework.http.client.ClientHttpRequestInterceptor
import org.springframework.http.client.ClientHttpResponse
import org.springframework.security.oauth2.client.OAuth2AuthorizeRequest
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientManager

class OAuthClientCredentialsInterceptor(
    private val manager: OAuth2AuthorizedClientManager,
    private val registrationId: String,
) : ClientHttpRequestInterceptor {

    override fun intercept(
        request: HttpRequest,
        body: ByteArray,
        execution: ClientHttpRequestExecution,
    ): ClientHttpResponse {
        val oAuth2AuthorizeRequest = OAuth2AuthorizeRequest
            .withClientRegistrationId(registrationId)
            .principal("anonymousUser")
            .build()
        val client = oAuth2AuthorizeRequest.authorize()
        request.headers.add(HttpHeaders.AUTHORIZATION, "Bearer " + client.accessToken.tokenValue)
        return execution.execute(request, body)
    }

    private fun OAuth2AuthorizeRequest.authorize() = manager.authorize(this)
        ?: throw NullPointerException("client credentials flow on $registrationId failed, client is null")
}
