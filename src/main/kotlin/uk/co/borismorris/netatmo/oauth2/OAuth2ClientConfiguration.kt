@file:Suppress("DEPRECATION")

package uk.co.borismorris.netatmo.oauth2

import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.security.oauth2.client.OAuth2ClientProperties
import org.springframework.boot.autoconfigure.security.oauth2.client.OAuth2ClientPropertiesMapper
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.converter.FormHttpMessageConverter
import org.springframework.security.oauth2.client.AuthorizedClientServiceOAuth2AuthorizedClientManager
import org.springframework.security.oauth2.client.OAuth2AuthorizationContext
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientManager
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientProvider
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientProviderBuilder
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientService
import org.springframework.security.oauth2.client.endpoint.DefaultPasswordTokenResponseClient
import org.springframework.security.oauth2.client.endpoint.DefaultRefreshTokenTokenResponseClient
import org.springframework.security.oauth2.client.http.OAuth2ErrorResponseErrorHandler
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository
import org.springframework.security.oauth2.client.registration.InMemoryClientRegistrationRepository
import org.springframework.security.oauth2.core.OAuth2AccessToken
import org.springframework.security.oauth2.core.endpoint.DefaultMapOAuth2AccessTokenResponseConverter
import org.springframework.security.oauth2.core.endpoint.OAuth2AccessTokenResponse
import org.springframework.security.oauth2.core.http.converter.OAuth2AccessTokenResponseHttpMessageConverter
import org.springframework.web.client.RestTemplate
import uk.co.borismorris.netatmo.config.props.NetatmoProps
import java.nio.file.Path
import java.util.*

@Configuration
@EnableConfigurationProperties(OAuth2ClientProperties::class)
class OAuth2ClientConfiguration {
    private val defaultAccessTokenConverter = DefaultMapOAuth2AccessTokenResponseConverter()

    @Bean
    fun clientRegistrationRepository(properties: OAuth2ClientProperties) =
        InMemoryClientRegistrationRepository(ArrayList(OAuth2ClientPropertiesMapper(properties).asClientRegistrations().values))

    @Bean
    fun oauth2AuthorizedClientService(
        registrationRepository: ClientRegistrationRepository,
        mapper: ObjectMapper,
        @Value("\${netatmo.cache-path}") path: Path,
    ) =
        JsonFileOauth2AuthorizedClientService(registrationRepository, mapper, path)

    @Bean
    fun oauth2AuthorizedClientManager(
        netatmoProps: NetatmoProps,
        registrationRepository: ClientRegistrationRepository,
        authorizedClientService: OAuth2AuthorizedClientService,
        authorizedClientProvider: OAuth2AuthorizedClientProvider,
    ): OAuth2AuthorizedClientManager {
        val passwordAttributes = with(netatmoProps.credentials) {
            mapOf(
                OAuth2AuthorizationContext.USERNAME_ATTRIBUTE_NAME to username,
                OAuth2AuthorizationContext.PASSWORD_ATTRIBUTE_NAME to password,
            )
        }
        val attributesMapper =
            AuthorizedClientServiceOAuth2AuthorizedClientManager.DefaultContextAttributesMapper()
                .andThen { attr ->
                    (attr.asSequence() + passwordAttributes.asSequence()).associateBy(
                        { it.key },
                        { it.value },
                    )
                }

        return AuthorizedClientServiceOAuth2AuthorizedClientManager(
            registrationRepository,
            authorizedClientService,
        ).apply {
            setAuthorizedClientProvider(authorizedClientProvider)
            setContextAttributesMapper(attributesMapper)
        }
    }

    @Bean
    fun authorisedClientProvider(): OAuth2AuthorizedClientProvider =
        OAuth2AuthorizedClientProviderBuilder.builder()
            .refreshToken {
                it.accessTokenResponseClient(
                    DefaultRefreshTokenTokenResponseClient().apply {
                        setRestOperations(restTemplate())
                    },
                )
            }
            .password {
                it.accessTokenResponseClient(
                    DefaultPasswordTokenResponseClient().apply {
                        setRestOperations(restTemplate())
                    },
                )
            }
            .build()

    private fun restTemplate() = RestTemplate(
        listOf(
            FormHttpMessageConverter(),
            OAuth2AccessTokenResponseHttpMessageConverter().also {
                it.accessTokenConverter()
            },
        ),
    ).apply {
        setErrorHandler(OAuth2ErrorResponseErrorHandler())
    }

    private fun OAuth2AccessTokenResponseHttpMessageConverter.accessTokenConverter() {
        this.setAccessTokenResponseConverter { accessTokenConverter(it) }
    }

    /**
     * Netatmo returns a response that
     * - is missing the token_type property
     * - has the scope property as a JSON list not a space delimited String
     */
    private fun accessTokenConverter(params: MutableMap<String, Any>): OAuth2AccessTokenResponse? {
        params["token_type"] = OAuth2AccessToken.TokenType.BEARER.value
        (params["scope"] as? Iterable<*>)?.also {
            params["scope"] = it.joinToString(separator = " ", transform = Objects::toString)
        }
        return defaultAccessTokenConverter.convert(params)
    }
}
