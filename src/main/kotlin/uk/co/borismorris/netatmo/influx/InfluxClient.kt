package uk.co.borismorris.netatmo.influx

import com.influxdb.client.write.Point
import com.influxdb.client.write.WriteParameters
import com.influxdb.query.FluxTable

interface InfluxClient {
    fun query(query: String): List<FluxTable>

    fun writePoints(points: List<Point>, parameters: WriteParameters)
}
