package uk.co.borismorris.netatmo.client

import org.springframework.core.ParameterizedTypeReference
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.web.client.RestClient
import org.springframework.web.util.UriBuilder
import uk.co.borismorris.netatmo.config.props.NetatmoProps
import java.time.Instant

class RestClientNetatmoClient(
    restClientBuilder: RestClient.Builder,
    netatmoProps: NetatmoProps,
) : NetatmoClient {

    private val restClient = restClientBuilder
        .baseUrl(netatmoProps.url.toString())
        .defaultHeader(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE)
        .build()

    override fun queryStationData(deviceId: DeviceId): NetatmoResponse<StationDataBody> = restClient.get()
        .uri { it.path("/getstationsdata").apply { deviceId.ifPresent { id -> queryParam("device_id", id) } }.build() }
        .exchange { _, clientResponse ->
            if (clientResponse.statusCode.isError) {
                throw NetatmoException(clientResponse.bodyTo(NetatmoErrorResponse::class.java))
            }

            clientResponse.bodyTo(object : ParameterizedTypeReference<NetatmoResponse<StationDataBody>>() {})
                ?: throw NullPointerException("Failed to get netatmo response")
        }

    override fun <T : DataObservation> queryDeviceMetrics(device: NetatmoDevice<T>, range: ClosedRange<Instant>): NetatmoResponse<NetatmoDeviceMetrics<T>> =
        restClient.get()
            .uri { it.path("/getmeasure").apply { buildMetricsQueryParams(device, range) }.build() }
            .exchange { _, clientResponse ->
                if (clientResponse.statusCode.isError) {
                    throw NetatmoException(clientResponse.bodyTo(NetatmoErrorResponse::class.java))
                }

                clientResponse.bodyTo(object : ParameterizedTypeReference<NetatmoResponse<OptimizedMetricsBody>>() {})
                    ?: throw NullPointerException("Failed to get netatmo response")
            }
            .run { mapBody { toDeviceMetrics(device) } }

    private fun UriBuilder.buildMetricsQueryParams(device: NetatmoDevice<*>, range: ClosedRange<Instant>) =
        queryParam("scale", "max")
            .queryParam("optimize", "true")
            .queryParam("real_time", "true")
            .queryParam("limit", 1024)
            .queryParam("date_begin", range.start.epochSecond)
            .queryParam("date_end", range.endInclusive.epochSecond)
            .queryParam(
                "type",
                device.availableMeasurements.flatMap { it.measurements }
                    .joinToString(separator = ",") { it.measurement },
            )
            .deviceId(device)

    private fun UriBuilder.deviceId(device: NetatmoDevice<*>) = when (device) {
        is BaseStation -> queryParam("device_id", device.id.asString())
        else -> queryParam("device_id", (device as AdditionalDevice).baseStation.id.asString()).queryParam(
            "module_id",
            device.id.asString(),
        )
    }

    fun <FROM : NetatmoResponseBody, TO : NetatmoResponseBody> NetatmoResponse<FROM>.mapBody(mapper: FROM.() -> TO) =
        NetatmoResponse(status, executionTime, serverTime, mapper(body))

    fun <T : DataObservation> NetatmoMetrics.toDeviceMetrics(device: NetatmoDevice<T>) =
        object : NetatmoDeviceMetrics<T> {
            override val module: NetatmoDevice<T>
                get() = device

            override fun metrics() = this@toDeviceMetrics.metrics().map { toDeviceMetric(it) }

            private fun toDeviceMetric(metric: NetatmoMetric) = device.availableMeasurements.asSequence()
                .flatMap { it.measurements.asSequence() }
                .map { it.measurement }
                .zip(metric.values.asSequence())
                .let { device.createMeasurementFromSequence(it + (timestamp.measurement to metric.time.epochSecond)) }

            override fun toString() = "NetatmoDeviceMetrics(device=$device,metrics=${this@toDeviceMetrics})"
        }
}
