package uk.co.borismorris.netatmo.client

import java.time.Instant
import java.util.Locale

interface NetatmoClient {

    fun queryStationData(deviceId: DeviceId = DeviceId.empty()): NetatmoResponse<StationDataBody>

    fun <T : DataObservation> queryDeviceMetrics(device: NetatmoDevice<T>, range: ClosedRange<Instant>): NetatmoResponse<NetatmoDeviceMetrics<T>>
}

class DeviceId private constructor(private val hardwareAddress: List<Byte>) {

    companion object {
        private val empty = DeviceId(listOf())

        fun empty() = empty

        fun from(vararg hardwareAddress: Byte) = from(hardwareAddress.toList())

        fun from(hardwareAddress: List<Byte>): DeviceId {
            assert(hardwareAddress.size == 6) { "MAC address must 6 bytes." }
            return DeviceId(hardwareAddress)
        }

        fun from(hardwareAddress: String) = from(hardwareAddress.split(":").map { it.toInt(16).toByte() })
    }

    private val stringForm: String by lazy {
        hardwareAddress.joinToString(separator = ":") { String.format(Locale.ROOT, "%02x", it).uppercase() }
    }

    fun isEmpty() = this == empty

    fun isNotEmpty() = !isEmpty()

    fun asString() = stringForm

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is DeviceId) return false

        if (hardwareAddress != other.hardwareAddress) return false

        return true
    }

    override fun hashCode(): Int {
        return hardwareAddress.hashCode()
    }

    override fun toString() = asString()
}

fun DeviceId.ifPresent(whenPresent: (String) -> Unit) {
    if (isNotEmpty()) whenPresent(asString())
}
