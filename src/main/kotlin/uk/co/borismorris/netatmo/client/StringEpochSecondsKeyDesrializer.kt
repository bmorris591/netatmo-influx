package uk.co.borismorris.netatmo.client

import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.KeyDeserializer
import com.fasterxml.jackson.databind.module.SimpleModule
import java.time.Instant

class StringEpochSecondsKeyDesrializer : KeyDeserializer() {
    override fun deserializeKey(key: String?, ctxt: DeserializationContext?) = key?.let { Instant.ofEpochSecond(it.toLong()) }
}

val netatmoApiModule = SimpleModule("NetatmoApi").also {
    it.addKeyDeserializer(Instant::class.java, StringEpochSecondsKeyDesrializer())
}
