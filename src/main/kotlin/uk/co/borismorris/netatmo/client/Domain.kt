package uk.co.borismorris.netatmo.client

import com.fasterxml.jackson.annotation.JsonBackReference
import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonCreator.Mode.DELEGATING
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonManagedReference
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.annotation.JsonSubTypes
import com.fasterxml.jackson.annotation.JsonTypeInfo
import com.fasterxml.jackson.annotation.JsonValue
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer
import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import com.fasterxml.jackson.databind.deser.impl.NullsConstantProvider.nuller
import com.fasterxml.jackson.databind.deser.std.StdDeserializer
import uk.co.borismorris.netatmo.client.Anemometer.AnemometerDeviceMeasurement
import uk.co.borismorris.netatmo.client.BaseStation.BaseStationMeasurement
import uk.co.borismorris.netatmo.client.IndoorDevice.IndoorDeviceMeasurement
import uk.co.borismorris.netatmo.client.OutdoorDevice.OutdoorDeviceMeasurement
import uk.co.borismorris.netatmo.client.Pluviometer.PluviometerDeviceMeasurement
import java.time.Duration
import java.time.Instant
import java.time.ZoneId
import java.util.Locale
import java.util.TreeMap

data class NetatmoResponse<BODY : NetatmoResponseBody>(
    @JsonProperty("status")
    val status: String,
    @JsonProperty("time_exec")
    @JsonDeserialize(using = DurationFromFractionalSeconds::class)
    val executionTime: Duration,
    @JsonProperty("time_server")
    val serverTime: Instant,
    @JsonProperty("body")
    val body: BODY,
)

interface NetatmoResponseBody

data class StationDataBody(
    @JsonProperty("devices")
    val devices: List<BaseStation>,
    @JsonProperty("user")
    val user: NetatmoUser,
) : NetatmoResponseBody

@JsonTypeInfo(
    use = JsonTypeInfo.Id.NAME,
    include = JsonTypeInfo.As.PROPERTY,
    property = "type",
    visible = true,
)
@JsonSubTypes(
    JsonSubTypes.Type(value = BaseStation::class, name = BASE_STATION),
    JsonSubTypes.Type(value = OutdoorDevice::class, name = OUTDOOR),
    JsonSubTypes.Type(value = Anemometer::class, name = WIND_GAUGE),
    JsonSubTypes.Type(value = Pluviometer::class, name = RAIN_GAUGE),
    JsonSubTypes.Type(value = IndoorDevice::class, name = INDOOR),
)
sealed class NetatmoDevice<T : DataObservation> {
    @JsonProperty("_id")
    @JsonDeserialize(using = DeviceIdFromString::class)
    lateinit var id: DeviceId

    @JsonProperty("type")
    lateinit var type: ModuleType

    @JsonProperty("module_name")
    lateinit var moduleName: String

    @JsonProperty("data_type")
    lateinit var availableMeasurements: List<MeasurementType>

    abstract val measurements: T?

    abstract fun createMeasurementFromSequence(seq: Sequence<Pair<String, Any>>): T

    @JsonProperty("last_setup")
    lateinit var lastSetupAt: Instant

    @JsonProperty("firmware")
    var firmwareVersion = Int.MIN_VALUE

    @JsonProperty(value = "last_upgrade", required = false)
    var lastUpgradeDate: Instant? = null

    @JsonProperty("reachable")
    var reachable = false

    @JsonProperty("co2_calibrating")
    var co2SensorCalibraring = true

    override fun toString() = "NetatmoDevice(id=$id, type=$type, moduleName='$moduleName', availableMeasurements=$availableMeasurements, measurements=$measurements, lastSetupAt=$lastSetupAt, firmwareVersion=$firmwareVersion, lastUpgradeDate=$lastUpgradeDate, reachable=$reachable, co2SensorCalibraring=$co2SensorCalibraring)"
}

class BaseStation : NetatmoDevice<BaseStationMeasurement>() {
    @JsonProperty("station_name")
    lateinit var name: String

    @JsonProperty("date_setup")
    lateinit var createdAt: Instant

    @JsonProperty("last_status_store")
    lateinit var lastStatusStoreAt: Instant

    @JsonProperty("wifi_status")
    var wifiStatus = Int.MIN_VALUE

    @JsonProperty("place")
    lateinit var location: DeviceLocation

    @JsonProperty("read_only")
    var readOnly = true

    @JsonProperty("dashboard_data")
    override var measurements: BaseStationMeasurement? = null

    override fun createMeasurementFromSequence(seq: Sequence<Pair<String, Any>>) = BaseStationMeasurement.createFromSequence(seq)

    @JsonProperty("modules")
    @JsonManagedReference
    lateinit var modules: List<AdditionalDevice<out DataObservation>>

    override fun toString() = "BaseStation(name='$name', createdAt=$createdAt, lastStatusStoreAt=$lastStatusStoreAt, wifiStatus=$wifiStatus, location=$location, readOnly=$readOnly, modules=$modules) ${super.toString()}"

    class BaseStationMeasurement private constructor(override val data: TreeMap<String, Any>) :
        MapDataObservation,
        MapTemperatureData,
        MapCo2Data,
        MapHumidityData,
        MapNoiseData,
        MapAtmosphericPressureData {
        companion object {
            @JvmStatic
            @JsonCreator(mode = DELEGATING)
            fun createFromMap(map: Map<String, Any>) = BaseStationMeasurement(map.ignoreCaseMap())

            fun createFromSequence(seq: Sequence<Pair<String, Any>>) = BaseStationMeasurement(seq.toMap(TreeMap(String.CASE_INSENSITIVE_ORDER)))
        }

        override fun toString() = "BaseStationMeasurement(data=$data)"
    }
}

fun Map<String, Any>.getNumericMeasurement(measurement: Measurement) = getNumber(measurement.measurement)

fun Map<String, Any>.getInstant(measurement: Measurement) = getNumericMeasurement(measurement)?.let { Instant.ofEpochSecond(it.toLong()) }

fun Map<String, Any>.getNumber(measurement: String) = get(measurement) as Number?

fun <T : Any> Map<String, T>.ignoreCaseMap() = TreeMap<String, Any>(String.CASE_INSENSITIVE_ORDER).apply { putAll(this@ignoreCaseMap) }

sealed class AdditionalDevice<T : DataObservation> : NetatmoDevice<T>() {
    @JsonBackReference
    lateinit var baseStation: BaseStation

    @JsonProperty("battery_percent")
    var batteryPercent = Int.MIN_VALUE

    @JsonProperty("last_message")
    lateinit var lastMessage: Instant

    @JsonProperty("last_seen")
    lateinit var lastSeen: Instant

    @JsonProperty("rf_status")
    val rfStatus = Int.MIN_VALUE

    @JsonProperty("battery_vp")
    val batteryVoltage = Int.MIN_VALUE

    override fun toString() = "AdditionalDevice(batteryPercent=$batteryPercent, lastMessage=$lastMessage, lastSeen=$lastSeen, rfStatus=$rfStatus, batteryVoltage=$batteryVoltage) ${super.toString()}"
}

class OutdoorDevice : AdditionalDevice<OutdoorDeviceMeasurement>() {
    @JsonProperty("dashboard_data")
    override var measurements: OutdoorDeviceMeasurement? = null

    override fun createMeasurementFromSequence(seq: Sequence<Pair<String, Any>>) = OutdoorDeviceMeasurement.createFromSequence(seq)

    override fun toString() = "OutdoorDevice() ${super.toString()}"

    class OutdoorDeviceMeasurement private constructor(override val data: TreeMap<String, Any>) :
        MapDataObservation,
        MapTemperatureData,
        MapHumidityData {
        companion object {
            @JvmStatic
            @JsonCreator(mode = DELEGATING)
            fun createFromMap(map: Map<String, Any>) = OutdoorDeviceMeasurement(map.ignoreCaseMap())

            fun createFromSequence(seq: Sequence<Pair<String, Any>>) = OutdoorDeviceMeasurement(seq.toMap(TreeMap(String.CASE_INSENSITIVE_ORDER)))
        }

        override fun toString() = "OutdoorDeviceMeasurement(data=$data)"
    }
}

class IndoorDevice : AdditionalDevice<IndoorDeviceMeasurement>() {
    @JsonProperty("dashboard_data")
    override var measurements: IndoorDeviceMeasurement? = null

    override fun createMeasurementFromSequence(seq: Sequence<Pair<String, Any>>) = IndoorDeviceMeasurement.createFromSequence(seq)

    override fun toString() = "IndoorDevice() ${super.toString()}"

    class IndoorDeviceMeasurement private constructor(override val data: TreeMap<String, Any>) :
        MapDataObservation,
        MapTemperatureData,
        MapCo2Data,
        MapHumidityData {
        companion object {
            @JvmStatic
            @JsonCreator(mode = DELEGATING)
            fun createFromMap(map: Map<String, Any>) = IndoorDeviceMeasurement(map.ignoreCaseMap())

            fun createFromSequence(seq: Sequence<Pair<String, Any>>) = IndoorDeviceMeasurement(seq.toMap(TreeMap(String.CASE_INSENSITIVE_ORDER)))
        }

        override fun toString() = "IndoorDeviceMeasurement(data=$data)"
    }
}

class Anemometer : AdditionalDevice<AnemometerDeviceMeasurement>() {
    @JsonProperty("dashboard_data")
    override var measurements: AnemometerDeviceMeasurement? = null

    override fun createMeasurementFromSequence(seq: Sequence<Pair<String, Any>>) = AnemometerDeviceMeasurement.createFromSequence(seq)

    override fun toString() = "Anemometer() ${super.toString()}"

    class AnemometerDeviceMeasurement private constructor(override val data: TreeMap<String, Any>) :
        MapDataObservation,
        MapWindData {
        companion object {
            @JvmStatic
            @JsonCreator(mode = DELEGATING)
            fun createFromMap(map: Map<String, Any>) = AnemometerDeviceMeasurement(map.ignoreCaseMap())

            fun createFromSequence(seq: Sequence<Pair<String, Any>>) = AnemometerDeviceMeasurement(seq.toMap(TreeMap(String.CASE_INSENSITIVE_ORDER)))
        }

        override fun toString() = "AnemometerDeviceMeasurement(data=$data)"
    }
}

class Pluviometer : AdditionalDevice<PluviometerDeviceMeasurement>() {
    @JsonProperty("dashboard_data")
    override var measurements: PluviometerDeviceMeasurement? = null

    override fun createMeasurementFromSequence(seq: Sequence<Pair<String, Any>>) = PluviometerDeviceMeasurement.createFromSequence(seq)

    override fun toString() = "Pluviometer() ${super.toString()}"

    class PluviometerDeviceMeasurement private constructor(override val data: TreeMap<String, Any>) :
        MapDataObservation,
        MapRainData {
        companion object {
            @JvmStatic
            @JsonCreator(mode = DELEGATING)
            fun createFromMap(map: Map<String, Any>) = PluviometerDeviceMeasurement(map.ignoreCaseMap())

            fun createFromSequence(seq: Sequence<Pair<String, Any>>) = PluviometerDeviceMeasurement(seq.toMap(TreeMap(String.CASE_INSENSITIVE_ORDER)))
        }

        override fun toString() = "PluviometerDeviceMeasurement(data=$data)"
    }
}

interface DataObservation {
    val observationDate: Instant
}

interface TemperatureData : DataObservation {
    val temperature: Double?
    val minTemperature: Double?
    val maxTemperature: Double?
    val minTemperatureDate: Instant?
    val maxTemperatureDate: Instant?
    val temperatureTrend: Trend?
}

interface HumidityData : DataObservation {
    val humidity: Double?
}

interface Co2Data : DataObservation {
    val co2ppm: Int?
}

interface NoiseData : DataObservation {
    val noiseDb: Double?
}

interface AtmosphericPressureData : DataObservation {
    val pressure: Double?
    val absolutePressure: Double?
    val pressureTrend: Trend?
}

interface RainData : DataObservation {
    val rainMm: Double?
    val sumRain1Hr: Double?
    val sumRain24Hr: Double?
}

interface WindData : DataObservation {
    val windStrength: Double?
    val windAngle: Double?
    val windGustStrength: Double?
    val windGustAngle: Double?
    val maxWindStrength: Double?
    val maxWindAngle: Double?
    val maxWindStrengthDate: Instant?
}

interface MapDataObservation : DataObservation {
    val data: Map<String, Any>

    override val observationDate: Instant
        get() = data.getInstant(timestamp) ?: error("No timestamp")
}

interface MapTemperatureData : MapDataObservation, TemperatureData {
    override val temperature: Double?
        get() = data.getNumericMeasurement(DetailMeasurement.Temperature)?.toDouble()
    override val minTemperature: Double?
        get() = TODO("Not yet implemented")
    override val maxTemperature: Double?
        get() = TODO("Not yet implemented")
    override val minTemperatureDate: Instant?
        get() = TODO("Not yet implemented")
    override val maxTemperatureDate: Instant?
        get() = TODO("Not yet implemented")
    override val temperatureTrend: Trend?
        get() = TODO("Not yet implemented")
}

interface MapHumidityData : MapDataObservation, HumidityData {
    override val humidity: Double?
        get() = data.getNumericMeasurement(DetailMeasurement.Humidity)?.toDouble()
}

interface MapCo2Data : MapDataObservation, Co2Data {
    override val co2ppm: Int?
        get() = data.getNumericMeasurement(DetailMeasurement.CO2)?.toInt()
}

interface MapNoiseData : MapDataObservation, NoiseData {
    override val noiseDb: Double?
        get() = data.getNumericMeasurement(DetailMeasurement.Noise)?.toDouble()
}

interface MapAtmosphericPressureData : MapDataObservation, AtmosphericPressureData {
    override val pressure: Double?
        get() = data.getNumericMeasurement(DetailMeasurement.Pressure)?.toDouble()
    override val absolutePressure: Double?
        get() = TODO("Not yet implemented")
    override val pressureTrend: Trend?
        get() = TODO("Not yet implemented")
}

interface MapWindData : MapDataObservation, WindData {
    override val windStrength: Double?
        get() = data.getNumericMeasurement(DetailMeasurement.WindStrength)?.toDouble()
    override val windAngle: Double?
        get() = data.getNumericMeasurement(DetailMeasurement.WindAngle)?.toDouble()
    override val windGustStrength: Double?
        get() = data.getNumericMeasurement(DetailMeasurement.GustStrength)?.toDouble()
    override val windGustAngle: Double?
        get() = data.getNumericMeasurement(DetailMeasurement.GustAngle)?.toDouble()
    override val maxWindStrength: Double?
        get() = TODO("Not yet implemented")
    override val maxWindAngle: Double?
        get() = TODO("Not yet implemented")
    override val maxWindStrengthDate: Instant?
        get() = TODO("Not yet implemented")
}

interface MapRainData : MapDataObservation, RainData {
    override val rainMm: Double?
        get() = data.getNumericMeasurement(DetailMeasurement.Rain)?.toDouble()
    override val sumRain1Hr: Double?
        get() = TODO("Not yet implemented")
    override val sumRain24Hr: Double?
        get() = TODO("Not yet implemented")
}

enum class Trend {
    Unknown,
    Up,
    Stable,
    Down, ;

    companion object {
        @JvmStatic
        @JsonCreator
        fun fromTrend(trend: String) = Trend.values()
            .find { it.name.lowercase() == trend }
            ?: throw IllegalArgumentException(trend)
    }
}

data class DeviceLocation(
    @JsonProperty("altitude")
    val altitude: Int,
    @JsonProperty("city")
    val city: String?,
    @JsonProperty("country")
    val country: Locale,
    @JsonProperty("timezone")
    val timeZone: ZoneId,
    @JsonProperty("location")
    val location: Position,
) {
    companion object {
        fun empty() = DeviceLocation(Int.MIN_VALUE, "", Locale.ROOT, ZoneId.systemDefault(), Position.unknown())
    }
}

const val BASE_STATION = "NAMain"
const val OUTDOOR = "NAModule1"
const val WIND_GAUGE = "NAModule2"
const val RAIN_GAUGE = "NAModule3"
const val INDOOR = "NAModule4"

enum class ModuleType(@JsonValue val type: String) {
    BaseStation(BASE_STATION),
    Outdoor(OUTDOOR),
    WindGauge(WIND_GAUGE),
    RainGauge(RAIN_GAUGE),
    Indoor(INDOOR),
}

enum class MeasurementType(val measurements: List<Measurement>) {
    Temperature(listOf(DetailMeasurement.Temperature)),
    CO2(listOf(DetailMeasurement.CO2)),
    Humidity(listOf(DetailMeasurement.Humidity)),
    Noise(listOf(DetailMeasurement.Noise)),
    Pressure(listOf(DetailMeasurement.Pressure)),
    Rain(listOf(DetailMeasurement.Rain)),
    Wind(listOf(DetailMeasurement.WindStrength, DetailMeasurement.WindAngle, DetailMeasurement.GustStrength, DetailMeasurement.GustAngle)),
}

interface Measurement {
    val measurement: String
}

val timestamp = object : Measurement {
    override val measurement = "time_utc"
}

enum class DetailMeasurement(override val measurement: String) : Measurement {
    Temperature("temperature"),
    CO2("co2"),
    Humidity("humidity"),
    Pressure("pressure"),
    Noise("noise"),
    Rain("rain"),
    WindStrength("windstrength"),
    WindAngle("windangle"),
    GustStrength("guststrength"),
    GustAngle("gustangle"),
}

@JsonDeserialize(using = PositionFromArray::class)
data class Position(val longitude: Double, val latitude: Double) {
    companion object {
        fun unknown() = Position(Double.NEGATIVE_INFINITY, Double.NEGATIVE_INFINITY)
    }
}

@JsonIgnoreProperties("administrative")
data class NetatmoUser(
    @JsonProperty("mail")
    val email: String,
)

interface NetatmoMetrics {
    fun metrics(): Sequence<NetatmoMetric>
}

interface NetatmoDeviceMetrics<T : DataObservation> : NetatmoResponseBody {
    val module: NetatmoDevice<T>

    fun metrics(): Sequence<T>
}

@JsonDeserialize(using = UnoptimizedMetricsDeserializer::class)
data class UnoptimizedMetricsBody(
    val data: Map<Instant, List<Double>>,
) : NetatmoResponseBody, NetatmoMetrics {

    override fun metrics() = data.entries
        .asSequence()
        .map { it.asMetric() }
}

@JsonDeserialize(using = OptimizedMetricsDeserializer::class)
data class OptimizedMetricsBody(
    val data: List<OptimizedMetricsData>,
) : NetatmoResponseBody, NetatmoMetrics {

    override fun metrics() = data
        .asSequence()
        .flatMap { it.metrics() }
}

data class OptimizedMetricsData(
    @JsonProperty("beg_time")
    val beginTime: Instant,
    @JsonProperty(value = "step_time", required = false)
    val step: Duration?,
    @JsonProperty("value")
    val observations: List<List<Double>>,
) : NetatmoMetrics {

    override fun metrics() = when (observations.size) {
        0 -> emptySequence()
        1 -> sequenceOf(NetatmoMetric(beginTime, observations[0]))
        else -> streamObservations()
    }

    @Suppress("UnsafeCallOnNullableType")
    private fun streamObservations(): Sequence<NetatmoMetric> {
        return observations
            .withIndex()
            .asSequence()
            .map { NetatmoMetric(beginTime.plus(step!!.multipliedBy(it.index.toLong())), it.value) }
    }
}

data class NetatmoMetric(
    val time: Instant,
    val values: List<Double>,
) {
    companion object {
        fun of(entry: Map.Entry<Instant, List<Double>>) = NetatmoMetric(entry.key, entry.value)
        fun of(pair: Pair<Instant, List<Double>>) = NetatmoMetric(pair.first, pair.second)
    }
}

fun Map.Entry<Instant, List<Double>>.asMetric() = NetatmoMetric.of(this)

fun Pair<Instant, List<Double>>.asMetric() = NetatmoMetric.of(this)

@JsonDeserialize(using = ErrorDeserializer::class)
data class NetatmoErrorResponse(val code: Int, val message: String)

class NetatmoException(val errorResponse: NetatmoErrorResponse?) : RuntimeException(errorResponse?.message ?: "Netatmo API request failed")

class DurationFromFractionalSeconds : StdDeserializer<Duration>(Duration::class.java) {
    override fun deserialize(p: JsonParser?, ctxt: DeserializationContext?): Duration = _parseDoublePrimitive(p, ctxt)
        .let { Duration.ofNanos((it * 1000000000).toLong()) }
}

class DeviceIdFromString : StdDeserializer<DeviceId>(DeviceId::class.java) {
    override fun deserialize(p: JsonParser?, ctxt: DeserializationContext?) = _parseString(p, ctxt, nuller())
        .let { DeviceId.from(it) }
}

class PositionFromArray : JsonDeserializer<Position>() {
    override fun deserialize(p: JsonParser?, ctxt: DeserializationContext?): Position {
        requireNotNull(p)
        requireNotNull(ctxt)
        val backingCollectionType = ctxt.typeFactory.constructCollectionLikeType(List::class.java, Double::class.java)
        val values = ctxt.readValue<List<Double>>(p, backingCollectionType)
        return Position(values[0], values[1])
    }
}

class UnoptimizedMetricsDeserializer : JsonDeserializer<UnoptimizedMetricsBody>() {
    override fun deserialize(p: JsonParser?, ctxt: DeserializationContext?): UnoptimizedMetricsBody {
        requireNotNull(p)
        requireNotNull(ctxt)

        val mapType = ctxt.typeFactory.constructMapLikeType(
            TreeMap::class.java,
            ctxt.typeFactory.constructSimpleType(Instant::class.java, arrayOf()),
            ctxt.typeFactory.constructCollectionLikeType(List::class.java, Double::class.java),
        )

        val data = ctxt.readValue<Map<Instant, List<Double>>>(p, mapType)

        return UnoptimizedMetricsBody(data)
    }
}

class OptimizedMetricsDeserializer : JsonDeserializer<OptimizedMetricsBody>() {
    override fun deserialize(p: JsonParser?, ctxt: DeserializationContext?): OptimizedMetricsBody {
        requireNotNull(p)
        requireNotNull(ctxt)

        val mapType = ctxt.typeFactory.constructCollectionLikeType(List::class.java, OptimizedMetricsData::class.java)

        val data = ctxt
            .readValue<List<OptimizedMetricsData>>(p, mapType)
            .sortedBy { it.beginTime }

        return OptimizedMetricsBody(data)
    }
}

class ErrorDeserializer : JsonDeserializer<NetatmoErrorResponse>() {
    companion object {
        private const val ERROR = "error"
        private const val CODE = "code"
        private const val MESSAGE = "message"
    }

    @Suppress("UNCHECKED_CAST")
    override fun deserialize(p: JsonParser?, ctxt: DeserializationContext?): NetatmoErrorResponse {
        requireNotNull(p)
        requireNotNull(ctxt)

        val mapType = ctxt.typeFactory.constructRawMapLikeType(HashMap::class.java)

        val data = ctxt.readValue<Map<String, Any>>(p, mapType)
        val error = data.getValue(ERROR) as Map<String, Any>
        return NetatmoErrorResponse(error.getValue(CODE) as Int, error.getValue(MESSAGE) as String)
    }
}
