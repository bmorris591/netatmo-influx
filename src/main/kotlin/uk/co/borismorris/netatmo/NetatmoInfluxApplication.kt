package uk.co.borismorris.netatmo

import com.influxdb.client.InfluxDBClientOptions
import io.micrometer.core.instrument.Metrics.globalRegistry
import io.micrometer.observation.ObservationRegistry
import io.pyroscope.javaagent.api.ConfigurationProvider
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.WebApplicationType
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.boot.autoconfigure.jackson.Jackson2ObjectMapperBuilderCustomizer
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.info.BuildProperties
import org.springframework.boot.info.GitProperties
import org.springframework.boot.runApplication
import org.springframework.boot.web.client.ClientHttpRequestFactories
import org.springframework.boot.web.client.ClientHttpRequestFactorySettings
import org.springframework.boot.web.client.RestClientCustomizer
import org.springframework.context.ConfigurableApplicationContext
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.core.env.Environment
import org.springframework.http.client.SimpleClientHttpRequestFactory
import org.springframework.scheduling.annotation.EnableScheduling
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientManager
import org.springframework.web.client.RestClient
import uk.co.borismorris.netatmo.client.NetatmoClient
import uk.co.borismorris.netatmo.client.RestClientNetatmoClient
import uk.co.borismorris.netatmo.client.netatmoApiModule
import uk.co.borismorris.netatmo.config.props.InfluxDB2Properties
import uk.co.borismorris.netatmo.config.props.NetatmoProps
import uk.co.borismorris.netatmo.influx.InfluxClient
import uk.co.borismorris.netatmo.influx.RestClientInfluxClient
import uk.co.borismorris.netatmo.logging.LogConfigOnStartup
import uk.co.borismorris.netatmo.logging.LogGitInfoOnStartup
import uk.co.borismorris.netatmo.logging.LogVersionOnStartup
import uk.co.borismorris.netatmo.oauth2.OAuthClientCredentialsInterceptor
import uk.co.borismorris.netatmo.pyroscope.Pyroscope
import uk.co.borismorris.netatmo.pyroscope.SpringConfigProvider
import uk.co.borismorris.netatmo.worker.GatherNetatmoData
import java.time.Duration

@Suppress("TooManyFunctions", "LongParameterList")
@EnableConfigurationProperties(InfluxDB2Properties::class)
@SpringBootApplication
class NetatmoInfluxApplication {

    @ConfigurationProperties(prefix = "netatmo")
    @Bean
    fun netatmoProps() = NetatmoProps()

    @Bean
    fun jacksonCustomiser() = Jackson2ObjectMapperBuilderCustomizer {
        it.findModulesViaServiceLoader(true)
            .modulesToInstall(netatmoApiModule)
    }

    @Bean
    fun influxConfig(properties: InfluxDB2Properties) = InfluxDBClientOptions.builder()
        .apply {
            properties.url?.let { url(it) }
            properties.token?.let { authenticateToken(it.toCharArray()) }
        }
        .bucket(properties.bucket)
        .org(properties.org)
        .build()

    @Bean
    fun restClientCustomizer() = RestClientCustomizer {
        it.requestFactory(
            ClientHttpRequestFactories.get(
                SimpleClientHttpRequestFactory::class.java,
                ClientHttpRequestFactorySettings.DEFAULTS
                    .withConnectTimeout(Duration.ofMinutes(5))
                    .withReadTimeout(Duration.ofMinutes(5)),
            ),
        )
    }

    @Bean
    fun influxClient(influxConfig: InfluxDBClientOptions, restClientBuilder: RestClient.Builder) =
        RestClientInfluxClient(restClientBuilder, influxConfig)

    @Bean
    fun netatmoClient(
        restClientBuilder: RestClient.Builder,
        netatmoProps: NetatmoProps,
        authorizedClientManager: OAuth2AuthorizedClientManager,
        @Value("\${spring.security.oauth2.client.default-client}") defaultClient: String,
    ) = RestClientNetatmoClient(
        restClientBuilder.requestInterceptor(oauthFilter(authorizedClientManager, defaultClient)),
        netatmoProps,
    )

    @Bean
    fun gatherNetatmoData(
        observationRegistry: ObservationRegistry,
        netatmoClient: NetatmoClient,
        influxConfig: InfluxDBClientOptions,
        influxClient: InfluxClient,
    ) = GatherNetatmoData(observationRegistry, netatmoClient, influxConfig, influxClient)

    @Bean
    fun logConfigOnStartup(context: ConfigurableApplicationContext) = LogConfigOnStartup(context)

    @Bean
    fun logVersionOnStartup(buildProperties: BuildProperties) = LogVersionOnStartup(buildProperties)

    @Bean
    fun logGitInfoOnStartup(gitProperties: GitProperties) = LogGitInfoOnStartup(gitProperties)

    @Bean
    @ConditionalOnClass(name = ["io.opentelemetry.javaagent.OpenTelemetryAgent"])
    fun otelRegistry() =
        globalRegistry.registries.find { r -> r.javaClass.name.contains("OpenTelemetryMeterRegistry") }?.also {
            globalRegistry.remove(it)
        }

    @Configuration
    @ConditionalOnProperty(value = ["scheduling.enabled"], havingValue = "true", matchIfMissing = true)
    @EnableScheduling
    class Scheduling

    @Configuration
    @ConditionalOnProperty(prefix = "pyroscope", name = ["enabled"], matchIfMissing = true)
    class PyroscopeConfig {
        @Bean
        fun springConfigProvider(environment: Environment) = SpringConfigProvider(environment)

        @Bean
        fun pyroscope(configurationProvider: ConfigurationProvider) = Pyroscope(configurationProvider)
    }
}

@Suppress("SpreadOperator", "TooGenericExceptionCaught")
fun main(args: Array<String>) {
    runApplication<NetatmoInfluxApplication>(*args) {
        webApplicationType = WebApplicationType.NONE
    }
}

private fun oauthFilter(authorizedClientManager: OAuth2AuthorizedClientManager, defaultClient: String) =
    OAuthClientCredentialsInterceptor(authorizedClientManager, defaultClient)
