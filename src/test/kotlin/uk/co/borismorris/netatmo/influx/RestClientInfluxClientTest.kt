package uk.co.borismorris.netatmo.influx

import com.github.tomakehurst.wiremock.client.WireMock.exactly
import com.github.tomakehurst.wiremock.client.WireMock.postRequestedFor
import com.github.tomakehurst.wiremock.client.WireMock.urlPathEqualTo
import com.github.tomakehurst.wiremock.client.WireMock.verify
import com.github.tomakehurst.wiremock.common.ConsoleNotifier
import com.github.tomakehurst.wiremock.core.WireMockConfiguration
import com.github.tomakehurst.wiremock.junit5.WireMockExtension
import com.influxdb.client.InfluxDBClientOptions
import com.influxdb.client.domain.WriteConsistency
import com.influxdb.client.domain.WritePrecision
import com.influxdb.client.write.Point
import com.influxdb.client.write.WriteParameters
import com.influxdb.query.dsl.Flux
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatThrownBy
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.RegisterExtension
import org.springframework.http.client.SimpleClientHttpRequestFactory
import org.springframework.web.client.RestClient
import java.time.Instant
import java.time.temporal.ChronoUnit
import java.util.function.Consumer

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class RestClientInfluxClientTest {

    @JvmField
    @RegisterExtension
    val wiremock = WireMockExtension.newInstance()
        .options(
            WireMockConfiguration.options()
                .dynamicPort()
                .usingFilesUnderClasspath("uk/co/borismorris/netatmo/influx/wiremock")
                .notifier(ConsoleNotifier(true)),
        )
        .configureStaticDsl(true)
        .build()

    lateinit var props: InfluxDBClientOptions
    lateinit var restClientBuilder: RestClient.Builder
    lateinit var client: RestClientInfluxClient

    @BeforeEach
    fun setupClient() {
        props = InfluxDBClientOptions.builder()
            .url(wiremock.baseUrl())
            .org("default")
            .bucket("testdb")
            .authenticateToken("abc123".toCharArray())
            .build()
        restClientBuilder = RestClient.builder()
            .requestFactory(SimpleClientHttpRequestFactory())
        client = RestClientInfluxClient(restClientBuilder, props)
    }

    @Test
    fun `When a single measurement is saved, it is serialised and transmitted`() {
        val measurement = createPoint("test-measurement")

        client.writePoints(listOf(measurement), WriteParameters(WritePrecision.MS, null))

        verify(exactly(1), postRequestedFor(urlPathEqualTo("/api/v2/write")))
    }

    @Test
    fun `When an error is encountered then it is returned to the user`() {
        val measurement = createPoint("error-measurement")

        assertThatThrownBy { client.writePoints(listOf(measurement), WriteParameters(WritePrecision.MS, null)) }
            .hasMessageContaining("unable to parse 'foo bar': bad timestamp")

        verify(exactly(1), postRequestedFor(urlPathEqualTo("/api/v2/write")))
    }

    @Test
    fun `When all the optional properties are set, they are sent as query params`() {
        val measurement = createPoint("options-measurement")

        client.writePoints(
            listOf(measurement),
            WriteParameters(null, null, WritePrecision.S, WriteConsistency.ANY),
        )

        verify(exactly(1), postRequestedFor(urlPathEqualTo("/api/v2/write")))
    }

    @Test
    fun `When a multiple measurements are saved, they are batched and transmitted`() {
        val measurement1 = createPoint("first-measurement")
        val measurement2 = createPoint("second-measurement")
        val measurement3 = createPoint("third-measurement")

        client.writePoints(
            listOf(measurement1, measurement2, measurement3),
            WriteParameters(WritePrecision.MS, null),
        )

        verify(exactly(1), postRequestedFor(urlPathEqualTo("/api/v2/write")))
    }

    @Test
    fun `When a flux query is sent then response is parsed`() {
        val result = client.query(Flux.from("testdb").range(-1, 1, ChronoUnit.DAYS).toString())

        val data = result.asSequence().flatMap { it!!.records }.toList()
        assertThat(data).hasSize(60)
        val tables = data.groupBy { it.table }
        assertThat(tables)
            .hasSize(3)
            .allSatisfy { tableId, table ->
                assertThat(tableId).isNotNull()
                assertThat(table).hasSize(20).allSatisfy(
                    Consumer {
                        assertThat(it.table).isEqualTo(tableId)
                    },
                )
            }

        verify(exactly(1), postRequestedFor(urlPathEqualTo("/api/v2/query")))
    }
}

fun createPoint(
    name: String,
    time: Instant = Instant.ofEpochMilli(1000),
    precision: WritePrecision = WritePrecision.NS,
) =
    Point(name).apply {
        time(time, precision)
        addTag("test-tag", "test-tag-value")
        addField("test-field", Math.PI)
    }
