package uk.co.borismorris.netatmo.client

import com.github.tomakehurst.wiremock.client.WireMock.exactly
import com.github.tomakehurst.wiremock.client.WireMock.getRequestedFor
import com.github.tomakehurst.wiremock.client.WireMock.postRequestedFor
import com.github.tomakehurst.wiremock.client.WireMock.urlPathEqualTo
import com.github.tomakehurst.wiremock.client.WireMock.verify
import com.github.tomakehurst.wiremock.common.Slf4jNotifier
import com.github.tomakehurst.wiremock.core.WireMockConfiguration.options
import com.github.tomakehurst.wiremock.junit5.WireMockExtension
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.RegisterExtension
import org.junit.jupiter.api.io.TempDir
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.annotation.DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.DynamicPropertyRegistry
import org.springframework.test.context.DynamicPropertySource
import uk.co.borismorris.netatmo.NetatmoInfluxApplication
import uk.co.borismorris.netatmo.client.MeasurementType.CO2
import uk.co.borismorris.netatmo.client.MeasurementType.Humidity
import uk.co.borismorris.netatmo.client.MeasurementType.Noise
import uk.co.borismorris.netatmo.client.MeasurementType.Pressure
import uk.co.borismorris.netatmo.client.MeasurementType.Temperature
import java.nio.file.Files
import java.nio.file.Path
import java.time.Instant
import kotlin.io.path.absolutePathString

@SpringBootTest(
    classes = [NetatmoInfluxApplication::class],
    webEnvironment = SpringBootTest.WebEnvironment.NONE,
    properties = [
        "pyroscope.enabled=false",
        "spring.security.oauth2.client.registration.netatmo.client-id=test-client-id",
        "spring.security.oauth2.client.registration.netatmo.client-secret=test-client-secret",
        "spring.security.oauth2.client.registration.netatmo.scope=test-scope-1,test-scope-2",
        "netatmo.credentials.username=test-user",
        "netatmo.credentials.password=test-user-password-123",
        "spring.codec.log-request-details=true",
        "scheduling.enabled=false",
    ],
)
@DirtiesContext(classMode = BEFORE_EACH_TEST_METHOD)
@ActiveProfiles("test")
internal class RestClientNetatmoClientTest {
    companion object {
        @JvmStatic
        @BeforeAll
        fun setupClass() {
            System.setProperty("javax.net.ssl.trustStore", "build/test-certs/truststore.p12")
            System.setProperty("javax.net.ssl.trustStorePassword", "password")
            System.setProperty("javax.net.ssl.trustStoreType", "PKCS12")
        }

        @JvmField
        @RegisterExtension
        val wiremock = WireMockExtension.newInstance()
            .options(
                options()
                    .dynamicPort()
                    .dynamicHttpsPort()
                    .keystorePath("build/test-certs/localhost.p12")
                    .keystorePassword("password")
                    .keystoreType("PKCS12")
                    .usingFilesUnderClasspath("uk/co/borismorris/netatmo/client/wiremock")
                    .notifier(Slf4jNotifier(true)),
            )
            .configureStaticDsl(true)
            .build()

        @JvmStatic
        @TempDir
        lateinit var tempDir: Path

        @JvmStatic
        @DynamicPropertySource
        fun registerDysonProperties(registry: DynamicPropertyRegistry) {
            registry.add("netatmo.cache-path") {
                Files.createTempDirectory(tempDir, "token-refresh").resolve("oauth_client_registry.json").absolutePathString()
            }
            registry.add("netatmo.url") { "${wiremock.baseUrl()}/api" }
            registry.add("spring.security.oauth2.client.provider.netatmo.token-uri") { "${wiremock.baseUrl()}/token" }
        }
    }

    val deviceId = DeviceId.from(0x70, 0xEE.toByte(), 0x50, 0x22, 0xA3.toByte(), 0x00)

    @Autowired
    lateinit var client: RestClientNetatmoClient

    @Test
    fun `When station data is requested then the user is authenticated`() {
        val stationData = client.queryStationData(deviceId)
        assertThat(stationData.status).isEqualTo("ok")

        verify(exactly(1), postRequestedFor(urlPathEqualTo("/token")))
        verify(exactly(1), getRequestedFor(urlPathEqualTo("/api/getstationsdata")))
    }

    @Test
    fun `When metrics are requested then the user is authenticated`() {
        val device = BaseStation().apply {
            id = deviceId
            availableMeasurements = listOf(Temperature, CO2, Humidity, Noise, Pressure)
        }
        val metrics =
            client.queryDeviceMetrics(device, Instant.ofEpochSecond(1586440184)..Instant.ofEpochSecond(1586524798))

        assertThat(metrics.status).isEqualTo("ok")
        assertThat(metrics.body.metrics().toList()).hasSize(281)

        verify(exactly(1), postRequestedFor(urlPathEqualTo("/token")))
        verify(exactly(1), getRequestedFor(urlPathEqualTo("/api/getmeasure")))
    }

    @Test
    fun `When multiple requests for stationdata are sent the user is authenticated once`() {
        repeat(5) {
            val stationData = client.queryStationData(deviceId)
            assertThat(stationData.status).isEqualTo("ok")
        }

        verify(exactly(1), postRequestedFor(urlPathEqualTo("/token")))
        verify(exactly(5), getRequestedFor(urlPathEqualTo("/api/getstationsdata")))
    }
}
