package uk.co.borismorris.netatmo.client

import com.github.tomakehurst.wiremock.client.WireMock.exactly
import com.github.tomakehurst.wiremock.client.WireMock.getAllScenarios
import com.github.tomakehurst.wiremock.client.WireMock.getRequestedFor
import com.github.tomakehurst.wiremock.client.WireMock.postRequestedFor
import com.github.tomakehurst.wiremock.client.WireMock.urlPathEqualTo
import com.github.tomakehurst.wiremock.client.WireMock.verify
import com.github.tomakehurst.wiremock.common.Slf4jNotifier
import com.github.tomakehurst.wiremock.core.WireMockConfiguration.options
import com.github.tomakehurst.wiremock.junit5.WireMockExtension
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.RegisterExtension
import org.junit.jupiter.api.io.TempDir
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.security.oauth2.client.OAuth2AuthorizeRequest
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientManager
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.annotation.DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.DynamicPropertyRegistry
import org.springframework.test.context.DynamicPropertySource
import uk.co.borismorris.netatmo.NetatmoInfluxApplication
import java.nio.file.Files
import java.nio.file.Path
import java.util.function.Consumer
import kotlin.io.path.absolutePathString

@SpringBootTest(
    classes = [NetatmoInfluxApplication::class],
    webEnvironment = SpringBootTest.WebEnvironment.NONE,
    properties = [
        "pyroscope.enabled=false",
        "spring.security.oauth2.client.registration.netatmo.client-id=test-client-id",
        "spring.security.oauth2.client.registration.netatmo.client-secret=test-client-secret",
        "spring.security.oauth2.client.registration.netatmo.scope=test-scope-1,test-scope-2",
        "netatmo.credentials.username=test-user-short-token",
        "netatmo.credentials.password=test-user-password-123-short-token",
        "scheduling.enabled=false",
    ],
)
@DirtiesContext(classMode = BEFORE_EACH_TEST_METHOD)
@ActiveProfiles("test")
internal class TokenRefreshRestClientNetatmoClientTest {
    companion object {
        @JvmStatic
        @BeforeAll
        fun setupClass() {
            System.setProperty("javax.net.ssl.trustStore", "build/test-certs/truststore.p12")
            System.setProperty("javax.net.ssl.trustStorePassword", "password")
            System.setProperty("javax.net.ssl.trustStoreType", "PKCS12")
        }

        @JvmField
        @RegisterExtension
        val wiremock = WireMockExtension.newInstance()
            .options(
                options()
                    .dynamicPort()
                    .dynamicHttpsPort()
                    .keystorePath("build/test-certs/localhost.p12")
                    .keystorePassword("password")
                    .keystoreType("PKCS12")
                    .usingFilesUnderClasspath("uk/co/borismorris/netatmo/client/wiremock")
                    .notifier(Slf4jNotifier(true)),
            )
            .configureStaticDsl(true)
            .build()

        @JvmStatic
        @TempDir
        lateinit var tempDir: Path

        @JvmStatic
        @DynamicPropertySource
        fun registerDysonProperties(registry: DynamicPropertyRegistry) {
            registry.add("netatmo.cache-path") {
                Files.createTempDirectory(tempDir, "token-refresh").resolve("oauth_client_registry.json").absolutePathString()
            }
            registry.add("netatmo.url") { "${wiremock.baseUrl()}/api" }
            registry.add("spring.security.oauth2.client.provider.netatmo.token-uri") { "${wiremock.baseUrl()}/token" }
        }
    }

    val deviceId = DeviceId.from(0x70, 0xEE.toByte(), 0x50, 0x22, 0xA3.toByte(), 0x00)

    @Autowired
    lateinit var client: RestClientNetatmoClient

    @Autowired
    lateinit var manager: OAuth2AuthorizedClientManager

    @Test
    fun `When a token expires it is refreshed`() {
        // Get a token that's already expired
        val oAuth2AuthorizeRequest = OAuth2AuthorizeRequest
            .withClientRegistrationId("netatmo")
            .principal("anonymousUser")
            .build()
        manager.authorize(oAuth2AuthorizeRequest)

        val stationData = client.queryStationData(deviceId)
        assertThat(stationData.status).isEqualTo("ok")

        verify(exactly(2), postRequestedFor(urlPathEqualTo("/token")))
        verify(exactly(1), getRequestedFor(urlPathEqualTo("/api/getstationsdata")))

        assertThat(getAllScenarios()).singleElement().satisfies(
            Consumer { scenario ->
                assertThat(scenario.state).isEqualTo("Finished")
            },
        )
    }
}
