package uk.co.borismorris.netatmo.oauth2

import com.fasterxml.jackson.databind.json.JsonMapper
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatCode
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.api.io.TempDir
import org.mockito.junit.jupiter.MockitoExtension
import org.springframework.security.authentication.AnonymousAuthenticationToken
import org.springframework.security.core.authority.AuthorityUtils.createAuthorityList
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient
import org.springframework.security.oauth2.client.registration.ClientRegistration
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository
import org.springframework.security.oauth2.client.registration.InMemoryClientRegistrationRepository
import org.springframework.security.oauth2.core.AuthorizationGrantType
import org.springframework.security.oauth2.core.OAuth2AccessToken
import org.springframework.security.oauth2.core.OAuth2AccessToken.TokenType.BEARER
import org.springframework.security.oauth2.core.OAuth2RefreshToken
import java.io.InputStreamReader
import java.nio.charset.StandardCharsets
import java.nio.file.Path
import java.time.Instant
import java.util.function.Consumer

const val REGISTRATION_ID = "registration"

@ExtendWith(MockitoExtension::class)
class JsonFileOauth2AuthorizedClientServiceTest {

    lateinit var clientRegistrationRepository: ClientRegistrationRepository
    lateinit var clientRegistration: ClientRegistration
    lateinit var authorizedClientService: JsonFileOauth2AuthorizedClientService

    @BeforeEach
    fun setup(@TempDir tmp: Path) {
        clientRegistration = ClientRegistration.withRegistrationId(REGISTRATION_ID)
            .authorizationGrantType(AuthorizationGrantType.CLIENT_CREDENTIALS)
            .clientId("Foo")
            .tokenUri("https://token.invalid")
            .build()
        clientRegistrationRepository = InMemoryClientRegistrationRepository(clientRegistration)
        val mapper = JsonMapper.builder().findAndAddModules().build()
        authorizedClientService = JsonFileOauth2AuthorizedClientService(
            clientRegistrationRepository,
            mapper,
            tmp.resolve("oauth_client_registry.json"),
        )
    }

    @Test
    fun `When no clients are saved load returns null`() {
        assertThat(authorizedClientService.loadAuthorizedClient<OAuth2AuthorizedClient>("Any", "Any"))
            .isNull()
    }

    @Test
    fun `I can save an authorized client`() {
        val client = oAuth2AuthorizedClient()
        val principal = authenticationToken()

        assertThatCode { authorizedClientService.saveAuthorizedClient(client, principal) }.doesNotThrowAnyException()
    }

    @Test
    fun `I can update an authorized client`() {
        val principal = authenticationToken()

        authorizedClientService.saveAuthorizedClient(oAuth2AuthorizedClient(), principal)

        val client = oAuth2AuthorizedClient(accessToken("AnotherTokenValue"))
        authorizedClientService.saveAuthorizedClient(client, principal)

        val loaded = authorizedClientService.loadAuthorizedClient<OAuth2AuthorizedClient>(REGISTRATION_ID, principal.name)!!

        assertThat(loaded.clientRegistration).isSameAs(clientRegistration)
        assertThat(loaded.principalName).isEqualTo(principal.name)
        assertThat(loaded.accessToken).isEqualTo(client.accessToken)
        assertThat(loaded.refreshToken).isEqualTo(client.refreshToken)
    }

    @Test
    fun `I can load an authorized client`() {
        val client = oAuth2AuthorizedClient()
        val principal = authenticationToken()

        authorizedClientService.saveAuthorizedClient(client, principal)

        val loaded =
            authorizedClientService.loadAuthorizedClient<OAuth2AuthorizedClient>(REGISTRATION_ID, principal.name)!!

        assertThat(loaded.clientRegistration).isSameAs(clientRegistration)
        assertThat(loaded.principalName).isEqualTo(principal.name)
        assertThat(loaded.accessToken).isEqualTo(client.accessToken)
        assertThat(loaded.refreshToken).isEqualTo(client.refreshToken)
    }

    @Test
    fun `I can delete an authorized client`() {
        val client = oAuth2AuthorizedClient()
        val principal = authenticationToken()

        authorizedClientService.saveAuthorizedClient(client, principal)

        val loaded = authorizedClientService.loadAuthorizedClient<OAuth2AuthorizedClient>(REGISTRATION_ID, principal.name)
        assertThat(loaded).isNotNull()

        authorizedClientService.removeAuthorizedClient(REGISTRATION_ID, principal.name)

        val deleted = authorizedClientService.loadAuthorizedClient<OAuth2AuthorizedClient>(REGISTRATION_ID, principal.name)
        assertThat(deleted).isNull()
    }

    @Test
    fun `I can read an old file`() {
        val clients =
            JsonFileOauth2AuthorizedClientService::class.java.getResourceAsStream("oauth_client_registry.json").use {
                authorizedClientService.read(InputStreamReader(it, StandardCharsets.UTF_8))
            }

        val client = oAuth2AuthorizedClient()
        assertThat(clients).singleElement().satisfies(
            Consumer {
                assertThat(it.clientRegistrationId).isEqualTo("registration")
                assertThat(it.principalName).isEqualTo("anonymousUser")
                assertThat(it.accessToken).isEqualTo(client.accessToken)
                assertThat(it.refreshToken).isEqualTo(client.refreshToken)
            },
        )
    }

    private fun authenticationToken() =
        AnonymousAuthenticationToken("key", "anonymousUser", createAuthorityList("USER"))

    private fun accessToken(tokenValue: String = "TokenValue") = OAuth2AccessToken(
        BEARER,
        tokenValue,
        Instant.EPOCH,
        Instant.MAX,
        setOf("ScopeOne", "ScopeTwo"),
    )

    private fun oAuth2AuthorizedClient(accessToken: OAuth2AccessToken = accessToken("TokenValue")): OAuth2AuthorizedClient {
        val client = OAuth2AuthorizedClient(
            clientRegistration,
            "anonymousUser",
            accessToken,
            OAuth2RefreshToken("RefreshValue", Instant.EPOCH, Instant.MAX),
        )
        return client
    }
}
