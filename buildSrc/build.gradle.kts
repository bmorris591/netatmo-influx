import org.jetbrains.kotlin.gradle.tasks.KotlinJvmCompile

plugins {
    `kotlin-dsl`
}

repositories {
    mavenCentral()
}

java {
    sourceCompatibility = JavaVersion.VERSION_17
    targetCompatibility = JavaVersion.VERSION_17
}

tasks.withType<KotlinJvmCompile> {
    kotlinOptions {
        jvmTarget = "17"
        freeCompilerArgs = listOfNotNull(
            "-Xjsr305=strict",
            "-Werror",
            "-opt-in=kotlin.RequiresOptIn",
            "-opt-in=kotlin.io.path.ExperimentalPathApi"
        )
    }
}

dependencies {
    implementation(libs.kotlin.logging)
    implementation(libs.bouncycastle)
}
