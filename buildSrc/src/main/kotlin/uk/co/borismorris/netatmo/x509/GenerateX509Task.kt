package uk.co.borismorris.netatmo.x509

import org.gradle.api.DefaultTask
import org.gradle.api.file.DirectoryProperty
import org.gradle.api.provider.Property
import org.gradle.api.provider.SetProperty
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.OutputDirectory
import org.gradle.api.tasks.TaskAction
import kotlin.io.path.createDirectories
import kotlin.io.path.isReadable

abstract class GenerateX509Task : DefaultTask() {

    @get:OutputDirectory
    abstract val outputDirectory: DirectoryProperty

    @get:Input
    abstract val rootCn: Property<String>

    @get:Input
    abstract val serverCn: Property<String>

    @get:Input
    abstract val domains: SetProperty<String>

    @get:Input
    abstract val serverKeypair: Property<String>

    @get:Input
    abstract val serverKeypairPassword: Property<String>

    @get:Input
    abstract val serverKeypairFormat: Property<String>

    @get:Input
    abstract val truststore: Property<String>

    @get:Input
    abstract val truststorePassword: Property<String>

    @get:Input
    abstract val truststoreFormat: Property<String>

    init {
        rootCn.convention("rootCA")
        serverCn.convention("localhost")
        domains.convention(mutableSetOf("localhost"))

        serverKeypair.convention("localhost.p12")
        serverKeypairPassword.convention("password")
        serverKeypairFormat.convention("PKCS12")

        truststore.convention("truststore.p12")
        truststorePassword.convention("password")
        truststoreFormat.convention("PKCS12")

        outputs.upToDateWhen {
            try {
                val truststore = outputDirectory.resolve(serverKeypair)
                if (!truststore.isReadable()) {
                    logger.info("No truststore cannot be up to date {}", truststore)
                    return@upToDateWhen false
                }
                return@upToDateWhen checkExpiryDate(truststore, truststoreFormat.get(), truststorePassword.get())
            } catch (ex: Exception) {
                logger.warn("Failed to determine task up to date", ex)
                return@upToDateWhen false
            }
        }
    }

    @TaskAction
    fun generateX509Certificates() {
        val file = outputDirectory.get().asFile.toPath()
        file.createDirectories()

        val root = generateCA()
        val server = generateCert(root)

        writeKeypair(outputDirectory.resolve(serverKeypair), serverKeypairFormat.get(), serverKeypairPassword.get(), server, root.certificate)
        writeTruststore(outputDirectory.resolve(truststore), truststoreFormat.get(), truststorePassword.get(), root.certificate)
    }

    private fun DirectoryProperty.resolve(child: Property<String>) = file(child).get().asFile.toPath()

    private fun generateCA() = createCertificate(rootCn.get(), emptySet(), null, true)

    private fun generateCert(issuer: GeneratedCert) = createCertificate(serverCn.get(), domains.get(), issuer, false)
}
