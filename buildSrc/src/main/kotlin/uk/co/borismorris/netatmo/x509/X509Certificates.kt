package uk.co.borismorris.netatmo.x509

import io.github.oshai.kotlinlogging.KotlinLogging
import org.bouncycastle.asn1.x500.X500Name
import org.bouncycastle.asn1.x509.BasicConstraints
import org.bouncycastle.asn1.x509.Extension.basicConstraints
import org.bouncycastle.asn1.x509.Extension.subjectAlternativeName
import org.bouncycastle.asn1.x509.GeneralName
import org.bouncycastle.asn1.x509.GeneralNames
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter
import org.bouncycastle.cert.jcajce.JcaX509v3CertificateBuilder
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder
import java.math.BigInteger
import java.nio.file.Path
import java.security.KeyPairGenerator
import java.security.KeyStore
import java.security.PrivateKey
import java.security.cert.X509Certificate
import java.time.Instant
import java.time.temporal.ChronoUnit
import java.util.Date
import kotlin.io.path.inputStream
import kotlin.io.path.outputStream

const val RSA = "RSA"
const val SHA256WithRSA = "SHA256WithRSA"

val logger = KotlinLogging.logger {}

data class GeneratedCert(val privateKey: PrivateKey, val certificate: X509Certificate)

fun createCertificate(cnName: String, domains: Set<String>, issuer: GeneratedCert?, isCA: Boolean): GeneratedCert {
    val certKeyPair = generateKeypair()
    val name = X500Name("CN=$cnName")

    val serialNumber = BigInteger.valueOf(System.currentTimeMillis())
    val validFrom = Instant.now()
    val validUntil = validFrom.plus(1, ChronoUnit.HOURS)

    val issuerName: X500Name
    val issuerKey: PrivateKey
    if (issuer == null) {
        issuerName = name
        issuerKey = certKeyPair.private
    } else {
        issuerName = X500Name(issuer.certificate.subjectX500Principal.name)
        issuerKey = issuer.privateKey
    }

    val cert = JcaX509v3CertificateBuilder(
        issuerName,
        serialNumber,
        Date.from(validFrom), Date.from(validUntil),
        name,
        certKeyPair.public
    ).run {
        if (isCA) {
            setCa()
        }
        domains.forEach { addSan(it) }
        buildAndSign(issuerKey)
    }

    return GeneratedCert(certKeyPair.private, cert)
}

private fun generateKeypair() = KeyPairGenerator.getInstance(RSA).generateKeyPair()

private fun JcaX509v3CertificateBuilder.buildAndSign(issuerKey: PrivateKey) = JcaContentSignerBuilder(SHA256WithRSA).build(issuerKey).let {
    JcaX509CertificateConverter().getCertificate(build(it))
}

private fun JcaX509v3CertificateBuilder.setCa() {
    addExtension(basicConstraints, true, BasicConstraints(true))
}

private fun JcaX509v3CertificateBuilder.addSan(domain: String) {
    addExtension(subjectAlternativeName, false, GeneralNames(GeneralName(GeneralName.dNSName, domain)))
}

fun writeKeypair(destination: Path, format: String, password: String, keypair: GeneratedCert, vararg chain: X509Certificate) {
    write(destination, format, password) {
        setKeyEntry(keypair.certificate.subjectX500Principal.name, keypair.privateKey, password.toCharArray(), arrayOf(keypair.certificate, *chain))
    }
}

fun writeTruststore(destination: Path, format: String, password: String, vararg roots: X509Certificate) {
    write(destination, format, password) {
        roots.forEach {
            setCertificateEntry(it.subjectX500Principal.name, it)
        }
    }
}

fun checkExpiryDate(destination: Path, format: String, password: String): Boolean {
    logger.info{"Checking keystore at $destination for expiry"}
    val keyStore = KeyStore.getInstance(format).apply {
        destination.inputStream().use {
            load(it, password.toCharArray())
        }
    }
    val now = Instant.now()
    val aliases = keyStore.aliases().toList()
    return aliases.asSequence()
        .map { keyStore.getCertificate(it) }
        .map { it as X509Certificate }
        .all { it.notAfter.toInstant().isAfter(now) }
}

fun write(destination: Path, format: String, password: String, populate: KeyStore.() -> Unit) {
    val keyStore = KeyStore.getInstance(format).apply {
        load(null, password.toCharArray())
    }

    keyStore.populate()

    destination.outputStream().use {
        keyStore.store(it, password.toCharArray())
    }
}
